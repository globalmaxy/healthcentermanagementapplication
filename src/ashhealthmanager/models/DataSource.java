/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ashhealthmanager.models;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author aladago
 */
public class DataSource {
    //ceate a static databse object. 
    //Make it final to prevent reasingment of opened databases

    private static final DataSource daoInstance = new DataSource();

    public static DataSource getDatabaseInstance() {
        return daoInstance;
    }
    // a connection instance for the class. 
    Connection conn = null;

    /**
     *
     */
    public void connectToDatabse() {
        final String USER_NAME = "GlobalMaxy";
        final String PASSWORD = "Globalmax@madMax";

        String dbURL = "jdbc:mysql://localhost/";
        Statement st;

        try {
            // Register driver
            Class.forName("com.mysql.jdbc.Driver");

            //obtain a connection
            conn = DriverManager.getConnection(dbURL, USER_NAME, PASSWORD);

            st = (Statement) conn.createStatement();

            //create pcs database if not exist.
            //use that database afterwards
            String createDbSql = "CREATE DATABASE IF NOT EXISTS ashHealthCenterDatabase";
            st.executeUpdate(createDbSql);
            st.executeUpdate("USE ashHealthCenterDatabase");

            //create patients database
            String createPateintsTbl = "CREATE TABLE IF NOT EXISTS "
                    + "patients("
                    + "patient_id VARCHAR(255) NOT NULL,"
                    + "first_name VARCHAR(255) NOT NULL,"
                    + "last_name VARCHAR(255) NOT NULL,"
                    + "email VARCHAR(255),"
                    + "PRIMARY KEY (patient_id)"
                    + ")";
            st.executeUpdate(createPateintsTbl);
            //create table to manage patitients vitals
            String createPateintsVitalsTbl = "CREATE TABLE IF NOT EXISTS "
                    + "patient_vitals("
                    + "patient_id VARCHAR(255) NOT NULL,"
                    + "temperature FLOAT NOT NULL,"
                    + "weight FLOAT NOT NULL,"
                    + "bp_systol FLOAT NOT NULL,"
                    + "bp_dystol FLOAT NOT NULL,"
                    + "PRIMARY KEY (patient_id, temperature, weight, bp_systol,bp_dystol),"
                    + "FOREIGN KEY(patient_id) REFERENCES patients(patient_id)"
                    + ")";
            st.executeUpdate(createPateintsVitalsTbl);
            //create table customers, may need to contact them later
            String createDrugsTbl = "CREATE TABLE IF NOT EXISTS "
                    + "drugs("
                    + "drug_id VARCHAR(255) NOT NULL,"
                    + "drug_name VARCHAR(255) NOT NULL,"
                    + "qty_available INTEGER,"
                    + "price_one FLOAT DEFAULT 0,"
                    + "PRIMARY KEY(drug_id)"
                    + ")";
            st.executeUpdate(createDrugsTbl);
            //create table for the records
            String createStaffTbl = "CREATE TABLE IF NOT EXISTS "
                    + "staff("
                    + "staff_id VARCHAR(255)NOT NULL,"
                    + "first_name VARCHAR(255) NOT NULL,"
                    + "last_name VARCHAR(255) NOT NULL,"
                    + "email VARCHAR(255) NOT NULL,"
                    + "PRIMARY KEY(staff_id)"
                    + ")";
            st.executeUpdate(createStaffTbl);

            String createTreatmentsTbl = "CREATE TABLE IF NOT EXISTS "
                    + "treatments("
                    + "tr_id INTEGER NOT NULL AUTO_INCREMENT,"
                    + "patient_id VARCHAR(255) NOT NULL,"
                    + "drug_id VARCHAR(255),"
                    + "qty_drug INTEGER DEFAULT 0,"
                    + "staff_id VARCHAR(255) NOT NULL,"
                    + "referred BOOLEAN DEFAULT FALSE,"
                    + "date date,"
                    + "PRIMARY KEY(tr_id),"
                    + "FOREIGN KEY(patient_id) REFERENCES patients(patient_id),"
                    + "FOREIGN KEY(drug_id) REFERENCES drugs(drug_id),"
                    + "FOREIGN KEY(staff_id) REFERENCES staff(staff_id)"
                    + ")";
            st.executeUpdate(createTreatmentsTbl);

            //create database for drug procurments
            String createProcurmentsTbl = "CREATE TABLE IF NOT EXISTS "
                    + "procurements("
                    + "pr_id VARCHAR(255) NOT NULL,"
                    + "drug_id VARCHAR(255) NOT NULL,"
                    + "procured_by VARCHAR(255) NOT NULL,"
                    + "qty INTEGER NOT NULL,"
                    + "date date,"
                    + "PRIMARY KEY (pr_id),"
                    + "FOREIGN KEY(procured_by) REFERENCES staff(staff_id),"
                    + "FOREIGN KEY(drug_id) REFERENCES drugs(drug_id)"
                    + ")";
            st.executeUpdate(createProcurmentsTbl);

        } catch (ClassNotFoundException | SQLException x) {
            x.printStackTrace();

        }

    }

    /**
     *
     * @param p_id
     * @param tmpt
     * @param wgt
     * @param bpSys
     * @param bpDys
     */
    public void insertIntoVitals(String p_id, float tmpt, float wgt,
            float bpSys, float bpDys) {
        String insertIntoVitals = "INSERT INTO patient_vitals "
                + "(patient_id, temperature, weight, bp_systol, bp_dystol) "
                + "VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoVitals)) {
            statement.setString(1, p_id);
            statement.setFloat(2, tmpt);
            statement.setFloat(3, wgt);
            statement.setFloat(4, bpSys);
            statement.setFloat(5, bpDys);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /*
    perform an insertion into patients database. A patient has an id
    name and an email address
     */
    public void insertIntoPatients(String p_id, String fistName, String lastName,
            String email) {
        String insertIntoPatients = "INSERT INTO patients "
                + "(patient_id, first_name, last_name, email) "
                + "VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoPatients)) {
            statement.setString(1, p_id);
            statement.setString(2, fistName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /*
    insertion into staff
     */
    public void insertIntoStaff(String s_id, String fistName, String lastName,
            String email) {
        String insertIntoStaff = "INSERT INTO staff "
                + "(staff_id, first_name, last_name, email) "
                + "VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoStaff)) {
            statement.setString(1, s_id);
            statement.setString(2, fistName);
            statement.setString(3, lastName);
            statement.setString(4, email);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param d_id
     * @param drugName
     * @param qty
     * @param price
     */
    public void addDrug(String d_id, String drugName, int qty, float price) {
        String insertIntoDrugs = "INSERT INTO drugs "
                + "(drug_id, drug_name, qty_available, price_one) "
                + "VALUES (?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoDrugs)) {
            statement.setString(1, d_id);
            statement.setString(2, drugName);
            statement.setInt(3, qty);
            statement.setFloat(4, price);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param tId
     * @param pId
     * @param dId
     * @param drugQty
     * @param referred
     * @param sId
     */
    public void insertIntoTreatments(String pId, String dId,
            int drugQty, boolean referred, String sId) {
        String insertIntoTreatments = "INSERT INTO treatments "
                + "(patient_id, drug_id, qty_drug, staff_id,referred, date) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoTreatments)) {
            statement.setString(1, pId);
            statement.setString(2, dId);
            statement.setInt(3, drugQty);
            statement.setString(4, sId);
            statement.setBoolean(6, referred);
            long d = System.currentTimeMillis();
            statement.setDate(7, new Date(d)); //insert with the current date
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param prId
     * @param dId
     * @param sId
     * @param qty
     */
    public void newProcurement(String prId, String dId, String sId, int qty) {
        String insertIntoDrugs = "INSERT INTO procurements "
                + "(pr_id, drug_id, procured_by, date, qty) "
                + "VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement
                = conn.prepareStatement(insertIntoDrugs)) {
            statement.setString(1, prId);
            statement.setString(2, dId);
            statement.setString(3, sId);
            long d = System.currentTimeMillis();
            statement.setDate(4, new Date(d));
            statement.setInt(5, qty);

            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * returns an arrraylist of all the patients in the records
     *
     * @return
     */
    public ArrayList<Object[]> getPatients() {
        ArrayList<Object[]> patients = new ArrayList<>();
        String getAllPatients = "SELECT * FROM patients";
        try (PreparedStatement statement
                = conn.prepareStatement(getAllPatients);
                ResultSet result = statement.executeQuery()) {
            /*
            retrieve all rows into an array list. The list will then ben converted
            to a 2d array
             */
            while (result.next()) {
                Object[] row = {
                    result.getString("patient_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("email")
                };
                patients.add(row);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return patients;
    }

    /**
     * get all the treatement so far
     *
     * @return
     */
    public ArrayList<Object[]> getAllTreaments() {
        ArrayList<Object[]> treatments = new ArrayList<>();
        String getAllTrements = "SELECT date, patients.last_name, drug_name, "
                + " staff.last_name, referred FROM treatments, patients,"
                + " drugs, staff WHERE treatments.staff_id = staff.staff_id AND"
                + " treatments.patient_id = patients.patient_id AND "
                + "treatments.drug_id = drugs.drug_id";
        try (PreparedStatement statement
                = conn.prepareStatement(getAllTrements);
                ResultSet result = statement.executeQuery()) {
            /*
            retrieve all rows into an array list. The list will then ben converted
            to a 2d array
             */
            while (result.next()) {
                Object[] row = {
                    result.getDate("date"),
                    result.getString("patients.last_name"),
                    result.getString("drug_name"),
                    result.getString("staff.last_name"),
                    result.getBoolean("referred")
                };
                treatments.add(row);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return treatments;
    }

    /**
     *
     * @return All the staff in the database.
     */
    public ArrayList<Object[]> getAllStaff() {
        //store all staff into arraylist
        ArrayList<Object[]> staff = new ArrayList<>();
        String getAllStaff = "SELECT * FROM staff ";
        try (PreparedStatement statement
                = conn.prepareStatement(getAllStaff);
                ResultSet result = statement.executeQuery()) {
            /*
            retrieve all rows into an array list. The list will then ben converted
            to a 2d array
             */
            while (result.next()) {
                Object[] row = {
                    result.getString("staff_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("email")
                };
                staff.add(row);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }

    /**
     *
     * @param p_id the id of patients whose vitals are desired
     * @return the vitals of a patient
     */
    public ArrayList<Object[]> getPatientVitals(String p_id) {
        //store all staff into arraylist
        ArrayList<Object[]> vitals = new ArrayList<>();
        String getVitals = "SELECT last_name, weight,temperature, "
                + "bp_systol, bp_dystol FROM patient_vitals, patients "
                + "WHERE patients.patient_id = ? AND "
                + "patient_vitals.patient_id = patients.patient_id";
        try (PreparedStatement statement
                = conn.prepareStatement(getVitals)) {
            statement.setString(1, p_id);
            try (ResultSet result = statement.executeQuery()){           
             /*
            retrieve all rows into an array list. The list will then ben converted
            to a 2d array
            */         
            while (result.next()) {
                        Object[] row = {
                            result.getString("last_name"),
                            result.getFloat("weight"),
                            result.getFloat("temperature"),
                            result.getFloat("by_systol"),
                            result.getFloat("bp_dystol")};
                        vitals.add(row);
                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
                return vitals;
            }
    /**
     * Delete a record from the patients table. Deletion may be rejected
     * if the patient Id is referenced in other tables.
     * @param patientId 
    
    public boolean deletePatient(String patientId){
        boolean deleteSuccess = false;
        String deleteQuery = "DELETE FROM patients where patient_id = ?";
        try (PreparedStatement ps = conn.prepareStatement(deleteQuery)) {
            ps.setString(1, patientId);
            int numRowsDeleted = ps.executeUpdate();
            if (numRowsDeleted > 0) {
                deleteSuccess = true;
            }

        } catch (SQLException ex) {
             ex.toString();
        }        
        return deleteSuccess;
    }  */
    /**
     * delete a recorded treatment from the database
     * @param tblName
     * @param patientId
     * @return  true if at least one record is deleted. Otherwise return false
     */
    public boolean deleteAllWithPatientID(String tblName, String patientId){
        boolean deleteSuccess = false;
        String deleteQuery = "DELETE FROM ? WHERE patient_id = ?";
        try (PreparedStatement ps = conn.prepareStatement(deleteQuery)){
            ps.setString(1, tblName);
            ps.setString(2, patientId);
            int numRowsDeleted = ps.executeUpdate();
            if (numRowsDeleted > 0) {
                deleteSuccess = true;
            }

        } catch (SQLException ex) {
             ex.toString();
        }        
        return deleteSuccess;
    }
    public boolean deleteATreatementID(long date, String patientId, String staffid){
        boolean deleteSuccess = false;
        String deleteQuery = "DELETE FROM treatments WHERE "
                + "patient_id = (SELECT patient_id FROM patients WHERE last_name = ?)"
                + " AND date = ? "
                + "AND staff_id = (SELECT staff_id FROM staff WHERE last_name =?)";
        try (PreparedStatement ps = conn.prepareStatement(deleteQuery)){
            ps.setDate(1, new Date(date));
            ps.setString(2, patientId); 
            ps.setString(3, staffid);            
            int numRowsDeleted = ps.executeUpdate();
            if (numRowsDeleted > 0) {
                deleteSuccess = true;
            }

        } catch (SQLException ex) {
             ex.toString();
        }        
        return deleteSuccess;
    }
    
            /**
             *
             * @param list
             * @return
             */

    public Object[][] get2DArray(ArrayList< Object[]> list) {
        if (list == null) {
            return null;
        }
        Object[][] twoDlist = new Object[list.size()][list.get(0).length];
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(0).length; j++) {
                twoDlist[i][j] = list.get(i)[j];
            }
        }
        return twoDlist;
    }
    

    /*
    close database connection
     */
    public void closeDatabase() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.err.println(ex.toString());
            }
        }
    }

}

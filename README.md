# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: 

This repository contains an entire netbeans project for managing a health center in a university. 

Upon start-up, the project displays a view of the patients currently in the center. However, there are also views for the available drugs, staff and treatments which can be navigated to using the 'Views' menu. 

On the patients view, a new patient can be added (admitted), deleted, have his/her details viewed, administered a full treatment or have his or vitals vitals taken. The details of a patient too can be edited. 
**
Special Note on deleting a patient or a staff or drug:** An attempted deletion of a patient or a drug or a staff may fail due to foreign key constraints. Since it is a hospital, all foreign keys are meant to restrict deletion or update of primary keys which have been referenced in other tables. For this reason, a patient 'Bright Awini' has been added to test deletion. 

*All treatments reports are written to a file called **treatments_report** at the root of the project*. Also, the details of all errors, especially those related to database configurations can be found in a file called *errors.txt* at the root of the project. 

* Version  0.01


### How do I get set up? ###


* Dependencies: No dependences are needed to run the project. A java SDK and netbeans are enough to have the project running. 
* Database configuration : The database is mysql using the JDBC driver. Please change** username and password at /src/ashhealthmanager/models/datasource** to match your own databse credentials. First run a dump of the database(all sql files) at **./DatabaseDump** before starting application. If you encounter any problem configuring the database, please check a file called *errors.txt* at the root of the project to find the specific error thrown. 
* How to run tests: No special tests have been added
* Deployment instructions : run project on netbeans or double click jar file at dist/ashealthmanager.jar